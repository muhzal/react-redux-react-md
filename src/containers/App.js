import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavigationDrawer from 'react-md/lib/NavigationDrawers';

import menus from 'components/AppMenu';

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ }, dispatch);
};

const mapStateToProps = ({ }) => ({
    
});

export class App extends Component {
    render() {
    	const { 
    		children,
    		// location: { pathname, search },
    	} = this.props;

        return (
          <NavigationDrawer
	        navItems={ menus() }
	        contentClassName="md-grid"
	        mobileDrawerType={ NavigationDrawer.DrawerTypes.TEMPORARY }
	        tabletDrawerType={ NavigationDrawer.DrawerTypes.PERSISTENT }
	        desktopDrawerType={ NavigationDrawer.DrawerTypes.PERSISTENT }
	        toolbarTitle="React Js"
	        contentId="main-content"
	      >
    		{ children }
  	    </NavigationDrawer>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
