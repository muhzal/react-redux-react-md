import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ }, dispatch);
};

const mapStateToProps = ({ }) => ({
    
});

export class Dashboard extends Component {
    render() {
        return (
			<h2>Dashboard</h2>            
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Dashboard);
