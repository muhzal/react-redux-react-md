import React, { Component, PropTypes } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';


const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({ }, dispatch);
};

const mapStateToProps = ({ }) => ({
    
});

export class User extends Component {
	componentDidMount() {

	}

    render() {
        return (
			<h2>User</h2>            
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(User);
