import React, { PropTypes } from 'react';
import logo from 'assets/logo.svg';

const AppHeader = ({ title }) => {	
	return (
		<div>			
			<img src={logo} className="md-logo" alt="logo" width="50" />
			<lable className="md-logo-text">{ title }</lable>			
		</div>
		);
};

AppHeader.displayName = 'AppHeader';

AppHeader.propTypes = {
    title: PropTypes.string,
};

export default AppHeader;
