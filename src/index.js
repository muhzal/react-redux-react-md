import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import WebFontLoader from 'webfontloader';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, browserHistory } from 'react-router';

import store from './store';
import routes from './routes';
import registerServiceWorker from './registerServiceWorker';
import './index.css';

WebFontLoader.load({
  google: {
    families: ['Roboto:300,400,500,700', 'Material Icons'],
  },
});

ReactDOM.render(
                <Provider store={store}>
                	<Router routes={routes} history={browserHistory} />
                </Provider>
                , document.getElementById('root'));


registerServiceWorker();
