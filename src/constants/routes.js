export default [{
  path: '',
  icon: 'home',
  primaryText: 'Home',
}, {
  path: 'user',
  icon: 'info_outline',
  primaryText: 'User',
}, {
  path: 'customization',
  icon: 'color_lens',
  nestedItems: [
    'colors',
    'themes',
    'media-queries',
    'grids',
    'typography', {
      primaryText: 'SassDoc',
      href: '/sassdoc',
    }, 'minimizing-bundle',
  ],
}, {
  path: 'discover-more',
  icon: 'search',
  nestedItems: [
    'whats-new',
    'showcases',
    'boilerplates', {
      path: 'upgrade-guides',
      nestedItems: [{
        path: 'v1.0.0',
        primaryText: 'Upgrading to v1.0.0',
      }, {
        path: 'v0.3.0',
        primaryText: 'Upgrading to v0.3.0',
      }],
    }, 'community',
    'contributing',
  ],
}, {
  path: 'components',
  icon: 'build',
  nestedItems: [
    'autocompletes',
    'avatars',
    'bottom-navigations',
    'buttons',
    'cards',
    'chips',
    'data-tables',
    'dialogs',
    'dividers',
    'drawers',
    'expansion-panels',
    'file-inputs',
    'font-icons', 
    {
      path: 'helpers',
      nestedItems: [
        'accessible-fake-button',
        'collapse',
        'focus-container',
        'icon-separator',
        'portal',
      ],
    },
    'inks',
    'lists',
    'media',
    'menus',
    'navigation-drawers',
    'papers', {
      path: 'pickers',
      nestedItems: ['date', 'time'],
    }, {
      path: 'progress',
      nestedItems: ['circular', 'linear'],
    }, 'select-fields', {
      path: 'selection-controls',
      nestedItems: ['selection-control', 'checkboxes', 'radios', 'switches'],
    },
    'sliders',
    'snackbars',
    'subheaders',
    'tabs',
    'text-fields',
    'toolbars',
    'tooltips',
  ],
}];
