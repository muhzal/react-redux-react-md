import App from 'containers/App';
import Dashboard from 'containers/Dashboard';
import User from 'containers/User';


const routes = {
  path: '/',
  component: App,
  indexRoute: { component: Dashboard },
  childRoutes: [
    { 
      path: 'user',
      component: User 
    },
  ]
}

export default routes;