import React, { Component } from 'react';
import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import { selectUser } from 'actions/actionUsers';

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({selectUser}, dispatch);
};

const mapStateToProps = ({ users }) => ({
    users,
});

export class UserList extends Component {
	createListItems(users){
		return users.map((user)=>{
			return (
			  	<li
				  	onClick={() => this.props.selectUser(user)}
				  	key={user.id}
			  	>
				  	{user.first} {user.last}
		  		</li>
			);
		});
	}

    render() {
        const {
            users
        } = this.props;

        return (
            <ul>
            	{this.createListItems(users)}
            </ul>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserList);
