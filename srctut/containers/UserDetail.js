import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

const mapStateToProps = ({ userActive }) => ({
    user : userActive,
});

export class UserDetail extends Component {
    render() {
        const {
            user
        } = this.props;

        return (
            <div>
            	<h2>{user.first} {user.last}</h2>
            	<h3>Age : {user.age}</h3>
            	<h3>Description : {user.description}</h3>
            </div>
        );
    }
}

export default connect(
    mapStateToProps
)(UserDetail);
