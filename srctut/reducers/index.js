import { combineReducers } from 'redux';
import users from './reducerUsers';
import userActive from './users.js';

const allReducers = combineReducers({
	users,
	userActive,
});

export default allReducers;