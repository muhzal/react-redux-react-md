const defaultState = {
    
};

const users = (state = defaultState, action = {}) => {
    switch (action.type) {
        case 'USER_SELECTED':
            return action.payload;
        default:
            return state;
    }
};

export default users;
