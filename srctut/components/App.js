import React from 'react';
import 'styles/app.css';
import UserList from 'containers/UserList';
import UserDetail from 'containers/UserDetail';
const App = ({ className }) => {
    return (
        <div>
        	<h2>Username List:</h2>
        	<UserList />
        	<hr/>
        	<h2>User Details:</h2>
        	<UserDetail />
        </div>
    );
};

export default App;
