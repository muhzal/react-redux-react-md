import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    auth as authAction,
} from '../actios/auth';

const mapDispatchToProps = (dispatch) => {
    return {
        auth: () => {
            dispatch(authAction());
        },
    };
};

const mapStateToProps = ({ state }) => ({
    auth: state.auth
});

export class Template extends Component {
    render() {
        const {
            auth
        } = this.props;

        return (
            <div></div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Template);
