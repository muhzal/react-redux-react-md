import React, { Component } from 'react';
import Toolbar from 'react-md/lib/Toolbars';
import logo from '../assets/logo.svg';
import '../index.scss';


export class App extends Component {

  render() {
    return (
      <Toolbar
        colored
        title="Welcome to React"
      />
    );
  }
}
