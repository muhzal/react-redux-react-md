// import cookie from 'js-cookie';

export default {
  auth: {
    isFetching: false,
    isAuthenticated: false //cookie.get('access_token') ? true : false
  },
  users: {
    isFetching: false,
    meta: {
      total: 0,
      perPage: 10,
      page: 1
    },
    data: []
  }
};
