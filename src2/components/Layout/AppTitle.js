import React, { Component } from 'react';
import { APP_TITLE } from '../../constants/app';
import logo from '../../assets/logo.svg';

export default ({ toolbarTitle, logo, ...props }) => {

	if(!logo) logo = <img src={logo} className="md-logo" alt="logo" width="50" />;
	if(!toolbarTitle) toolbarTitle	= APP_TITLE;

	return (
		<div>			
			{ logo }
			<lable className="md-logo-text">{ toolbarTitle }</lable>			
		</div>
    );
}
