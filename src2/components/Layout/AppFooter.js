import React, { Component } from 'react';
import cn from 'classnames';
import Button from 'react-md/lib/Buttons/Button';

export default class AppFooter extends Component {
	render() {
		return (
		    <footer className="md-grid app-footer">
		       <section className="md-cell md-cell--4">
			    <h4 className="md-title">Contact</h4>
			    <Button flat primary label="Muhammad Rizal" href="mailto:muhzal94@gmail.com">mail</Button>
			  </section>
		      <p className="md-text-right md-cell md-cell--12">Current version: <i>0.0.1</i></p>
		    </footer>
		);
	}
}
