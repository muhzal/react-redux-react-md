import React, { Component } from 'react';
import NavigationDrawer from 'react-md/lib/NavigationDrawers';
import menus from '../../constants/menus';

export default class AppMenu extends Component {
	constructor(props) {
		super(props);
		this._navItems = menus;
	}
	render() {
		{ toolbarTitle, ...props } = this.props;

		return (
	      <NavigationDrawer	      	
	        navItems={this._navItems}
	        defaultVisible={true}
	        persistentIconChildren="close"
	        includeDrawerHeader={false}
	        // renderNode={dialog}
	        // contentClassName="md-grid"
	        // drawerHeaderChildren={drawerHeaderChildren}
	        mobileDrawerType={NavigationDrawer.DrawerTypes.TEMPORARY}
	        tabletDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT}
	        desktopDrawerType={NavigationDrawer.DrawerTypes.PERSISTENT}
	        toolbarTitle={ toolbarTitle }
	        // toolbarActions={closeButton}
	        // contentId="main-content-demo"
	      >
	      	{ this.props.children }
	       </NavigationDrawer>
		);
	}
}
